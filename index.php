<?php
require_once('animal.php');
require_once('Frog.php');
require_once('Ape.php');


$sheep = new Animal("shaun");
echo "Name : ".$sheep->name."<br>"; // "shaun"
echo "legs : ".$sheep->legs."<br>"; // 4
echo "cold blooded : ".$sheep->cold_blooded."<br><br>"; // "no"

$bangkong = new Frog("buduk");
echo "Name : ".$bangkong->name."<br>"; // "shaun"
echo "legs : ".$bangkong->legs."<br>"; // 4
echo "cold blooded : ".$bangkong->cold_blooded."<br>";
echo $bangkong->loncat()."<br><br>";

$ape2 = new Ape("kera sakti");
echo "Name : ".$ape2->name."<br>"; // "shaun"
echo "legs : ".$ape2->legs."<br>"; // 4
echo "cold blooded : ".$ape2->cold_blooded."<br>";
echo $ape2->teriak()."<br>";

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>